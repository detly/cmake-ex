# On Windows

    mkdir build
    cd build
    cmake ..
    cmake --build .
    cmake --build . --target "magic"

Doesn't work. Need to use `cmake --build "modules/magic" --target "magic"`.

# On Linux

    mkdir build
    cd build
    cmake ..
    cmake --build .
    cmake --build . --target "magic"

Works fine.
